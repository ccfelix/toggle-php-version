# Toggle PHP version

Permite alternar rapidamente as versões do PHP na linha de comando.

O path é alterado na memória da janela do shell ativo.
Ao fechar a versão padrão configurada no path é restaurada.

Nos nossos arquivos, alternamos entre a versão PHP 5.6,
que no path deve estar numa pasta "php56" e o PHP 7.1 que
deve estar numa pasta "php71".

OS dois arquivos do repositório devem estar em algum caminho 
válido no path

No cmd chamamos usando: 

use-php7 

para carregar o PHP 7 na memória do prompt.

Para um shell como do git chamamos usando:

. use-php7

para carregar o PHP 7 na memória do shell.

O uso do ponto (.) é para indicar ao shell que
o comando deve executar na instância atual do mesmo. 
